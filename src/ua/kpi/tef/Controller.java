package ua.kpi.tef;

import java.util.Scanner;

public class Controller {

    // Constructor
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    // The Work method
    public void processUser() {
        Scanner sc = new Scanner(System.in);

        do {
            model.setUserValue(inputIntValueWithScanner(sc));
            view.printIntAndMessage(model.getUserValue(), getStatusMessage());
        } while (model.compare() != 0);

        view.printIntAndMessage(model.getStats(), View.STATS);
    }

    private String getStatusMessage() {
        int diff = model.compare();
        if (diff == 0){
            return View.WIN;
        }

        return diff < 0 ? View.DOWN : View.UP;
    }

    // The Utility methods
    public int inputIntValueWithScanner(Scanner sc) {
        view.printMessage(View.INPUT_INT_DATA);
        while (!sc.hasNextInt()) {
            view.printMessage(View.WRONG_INPUT_INT_DATA + View.INPUT_INT_DATA);
            sc.next();
        }
        return sc.nextInt();
    }
}
