package ua.kpi.tef;

public class View {
    // Text's constants
    public static final String INPUT_INT_DATA = "Input INT value = ";
    public static final String WRONG_INPUT_INT_DATA = "Wrong input! Repeat please! ";
    public static final String WIN = " Win!";
    public static final String DOWN = " - to small";
    public static final String UP = " - to high";
    public static final String STATS = " - number of try";

    public void printMessage(String message){
        System.out.println(message);
    }

    public void printIntAndMessage(int value, String message){
        System.out.println(value + message);
    }

}
