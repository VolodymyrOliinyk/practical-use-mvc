package ua.kpi.tef;

public class Model {
    private static final int RAND_MAX = 100;
    private int valueUser;
    private final int valueSystem;
    private int counter = 0;

    public Model() {
        valueSystem = rand();
    }

    private int rand() {
        return rand(0, RAND_MAX);
    }

    private int rand(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    // The Program logic

    public void setUserValue(int value) {
        counter++;
        this.valueUser = value;
    }

    public int compare() {
        return valueUser - valueSystem;
    }

    public int getUserValue() {
        return valueUser;
    }

    public int getStats() {
        return counter;
    }
}
